import Payslip from "../../../lib/domain/entities/Payslip";
describe("Payslip", () => {
  test("Calculate Tax Amount", () => {
    //given
    const payslip = new Payslip(
      1,
      "97084172E",
      new Date(),
      2486, //gross
      5, // insurance %
      124.3, //insurance
      12, //tax %
      298.32, // tax
      2063.37 // net
    );

    //when
    payslip.calculateTaxAmount(10);

    //then
    expect(payslip.taxAmount).toBe(248.6);
    expect(payslip.net).toBe(2113.1);
  });

  test("Calculate Tax Amount - tax rate lesser than 0", () => {
    //given

    const payslip = new Payslip(
      1,
      "97084172E",
      new Date(),
      2486, //gross
      5, // insurance %
      124.3, //insurance
      12, //tax %
      298.32, // tax
      2063.37 // net
    );
    expect(() => {
      //when
      payslip.calculateTaxAmount(-15);

      //then
    }).toThrowError("Tax rate cannot be lesser than 0");
  });

  test("Calculate Tax Amount - tax rate greater than 100", () => {
    //given
    const payslip = new Payslip(
      1,
      "97084172E",
      new Date(),
      2486, //gross
      5, // insurance %
      124.3, //insurance
      12, //tax %
      298.32, // tax
      2063.37 // net
    );
    expect(() => {
      //when
      payslip.calculateTaxAmount(400);

      //then
    }).toThrowError("Tax rate cannot be greater than 100");
  });

  test("Calculate Tax Amount - tax rate makes tax amount exceeds the net", () => {
    //given
    const payslip = new Payslip(
      1,
      "97084172E",
      new Date(),
      2486, //gross
      51, // insurance %
      1267.35, //insurance
      12, //tax %
      298.32, // tax
      2063.37 // net
    );
    expect(() => {
      //when
      payslip.calculateTaxAmount(50);

      //then
    }).toThrowError("New tax amount exceeds the net amount");
  });
});
