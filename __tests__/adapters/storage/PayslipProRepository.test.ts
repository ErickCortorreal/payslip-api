import PayslipProRepository from "../../../lib/adapters/storage/PayslipProRepository";
import Payslip from "../../../lib/domain/entities/Payslip";
import moment from "moment";

describe("PayslipProRepository", () => {
  test("parseLine", () => {
    //given
    const repo = new PayslipProRepository({
      apiURIs: new Map<string, never>()
    });
    //when
    const payslip = repo.parseLine(
      "00000000000197084172E201812310024860005000001243012000002983200206337"
    );

    //then
    expect(payslip).toEqual(
      new Payslip(
        1,
        "97084172E",
        moment("2018-12-31").toDate(),
        2486,
        5,
        124.3,
        12,
        298.32,
        2063.37
      )
    );
  });
  test("parseDecimal - ignoring in front 0s", () => {
    //given
    const repo = new PayslipProRepository({
      apiURIs: new Map<string, never>()
    });
    //when
    const decimal = repo.parseDecimal("100", 2);

    //then
    expect(decimal).toBe(1.0);
  });
  test("parseDecimal - ignoring in front 0s", () => {
    //given
    const repo = new PayslipProRepository({
      apiURIs: new Map<string, never>()
    });
    //when
    const decimal = repo.parseDecimal("00000000000112", 2);

    //then
    expect(decimal).toBe(1.12);
  });
  test("parseDecimal - with no scale", () => {
    //given
    const repo = new PayslipProRepository({
      apiURIs: new Map<string, never>()
    });
    //when
    const decimal = repo.parseDecimal("1");

    //then
    expect(decimal).toBe(1);
  });
});
