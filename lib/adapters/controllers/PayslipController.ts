import PayslipRepositoryFactory from "../storage/PayslipRepositoryFactory";
import IAdaptersConfigurations from "../IAdaptersConfigurations";
import FindPayslipByPeriod from "../../application/use_cases/FindPayslipByPeriod";
import CalculateTaxAmount from "../../application/use_cases/CalculateTaxAmount";

export default class PayslipController {
  repositoryFactory: PayslipRepositoryFactory;
  constructor(configs: IAdaptersConfigurations) {
    this.repositoryFactory = new PayslipRepositoryFactory(configs);
  }

  findByPeriod = (request: any) => {
    const repository = this.repositoryFactory.getRepository(
      request.headers.company
    );
    if (repository === null) {
      throw new Error("Unimplemented repository");
    }
    return new FindPayslipByPeriod(repository).execute(
      request.params.year,
      request.params.month
    );
  };

  updateTaxRateByPeriod = (request: any) => {
    const repository = this.repositoryFactory.getRepository(
      request.headers.company
    );
    if (repository === null) {
      throw new Error("Unimplemented repository");
    }
    return new FindPayslipByPeriod(repository)
      .execute(request.params.year, request.params.month)
      .then(payslips =>
        new CalculateTaxAmount(payslips).execute(request.payload.taxRate)
      );
  };
}
