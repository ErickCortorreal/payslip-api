import IPayslipRepository from "../../application/repositories/IPayslipRepository";
import IAdaptersConfigurations from "../IAdaptersConfigurations";
import Payslip from "../../domain/entities/Payslip";
import axios, { AxiosResponse } from "axios";
import moment from "moment";
export default class PayslipProRepository implements IPayslipRepository {
  configs: IAdaptersConfigurations;
  constructor(configs: IAdaptersConfigurations) {
    this.configs = configs;
  }

  async findByPeriod(year: number, month: number): Promise<Payslip[]> {
    const uri = this.configs.apiURIs.get("yellows_car");
    return axios.get(`${uri}${year}${month}.txt`).then(this.parseResponse);
  }

  parseResponse = (response: AxiosResponse) =>
    response.data.split("\n").map(this.parseLine);

  parseLine = (line: string) => {
    return new Payslip(
      Number(line.slice(0, 12)),
      line.slice(12, 21),
      moment(line.slice(21, 29), "YYYYMMDD").toDate(),
      this.parseDecimal(line.slice(29, 37), 2),
      this.parseDecimal(line.slice(37, 41), 2),
      this.parseDecimal(line.slice(41, 49), 2),
      this.parseDecimal(line.slice(49, 53), 2),
      this.parseDecimal(line.slice(53, 61), 2),
      this.parseDecimal(line.slice(61, 69), 2)
    );
  };

  parseDecimal(unparsedDecimal: string, scale: number = 0) {
    const intLength = unparsedDecimal.length - scale;
    return Number(
      `${unparsedDecimal.slice(0, intLength)}.${unparsedDecimal.slice(
        intLength,
        unparsedDecimal.length
      )}`
    );
  }
}
