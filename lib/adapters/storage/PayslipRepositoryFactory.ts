import IPayslipRepository from "../../application/repositories/IPayslipRepository";
import PayslipProRepository from "./PayslipProRepository";
import IAdaptersConfigurations from "../IAdaptersConfigurations";

export default class PayslipRepositoryFactory {
  configs: IAdaptersConfigurations;
  constructor(configs: IAdaptersConfigurations) {
    this.configs = configs;
  }
  getRepository = (type: string): IPayslipRepository | null => {
    switch (type) {
      case "yellows_car":
        return new PayslipProRepository(this.configs);
      default:
        return new PayslipProRepository(this.configs);
    }
  };
}
