import * as Hapi from "@hapi/hapi";
import Joi from "@hapi/joi";
import PayslipController from "../../../adapters/controllers/PayslipController";
import { getAdaptersConfigs } from "../configurations";

export default function(server: Hapi.Server) {
  const payslipController = new PayslipController(getAdaptersConfigs());
  server.route({
    method: "GET",
    path: "/payslips/{year}/{month}",
    options: {
      handler: payslipController.findByPeriod,
      tags: ["api", "payslips"],
      description: "Get paylisps by an specific period",
      validate: {
        headers: Joi.object({
          company: Joi.string()
            .allow("yellows_car")
            .required()
        }).options({ allowUnknown: true }),
        params: Joi.object({
          year: Joi.number().required(),
          month: Joi.number()
            .min(1)
            .max(12)
            .required()
        })
      },
      plugins: {
        "hapi-swagger": {
          responses: {
            "200": {
              description: "Payslips founded."
            }
          }
        }
      }
    }
  });
  server.route({
    method: "PUT",
    path: "/payslips/{year}/{month}",
    options: {
      handler: payslipController.updateTaxRateByPeriod,
      tags: ["api", "payslips"],
      description: "update paylisps by an specific period",
      validate: {
        headers: Joi.object({
          company: Joi.string()
            .allow("yellows_car")
            .required()
        }).options({ allowUnknown: true }),
        params: Joi.object({
          year: Joi.number().required(),
          month: Joi.number()
            .min(1)
            .max(12)
            .required()
        }),
        payload: Joi.object({
          taxRate: Joi.number()
            .min(0)
            .max(100)
        })
      },
      plugins: {
        "hapi-swagger": {
          responses: {
            "200": {
              description: "Payslips founded."
            }
          }
        }
      }
    }
  });
}
