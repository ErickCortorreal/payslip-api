export default class Payslip {
  id: number;
  identificationNumber: string;
  date: Date;
  gross: number;
  nationalInsuranceRate: number;
  nationalInsuranceAmount: number;
  taxRate: number;
  taxAmount: number;
  net: number;
  constructor(
    id: number,
    identificationNumber: string,
    date: Date,
    gross: number,
    nationalInsuranceRate: number,
    nationalInsuranceAmount: number,
    taxRate: number,
    taxAmount: number,
    net: number
  ) {
    this.id = id;
    this.identificationNumber = identificationNumber;
    this.date = date;
    this.gross = gross;
    this.nationalInsuranceRate = nationalInsuranceRate;
    this.nationalInsuranceAmount = nationalInsuranceAmount;
    this.taxRate = taxRate;
    this.taxAmount = taxAmount;
    this.net = net;
  }

  calculateTaxAmount(newTaxRate: number) {
    if (newTaxRate < 0) {
      throw "Tax rate cannot be lesser than 0";
    }
    if (newTaxRate > 100) {
      throw "Tax rate cannot be greater than 100";
    }

    this.taxRate = newTaxRate;
    this.taxAmount = (this.gross * this.taxRate) / 100;
    this.net = this.gross - this.taxAmount - this.nationalInsuranceAmount;
    if (this.net < 0) {
      throw "New tax amount exceeds the net amount";
    }
    return this;
  }
}
