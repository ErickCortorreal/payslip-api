import Payslip from "../../domain/entities/Payslip";

export default interface IPayslipRepository {
  findByPeriod(year: number, month: number): Promise<Array<Payslip>>;
}
