import IPayslipRepository from "../repositories/IPayslipRepository";

export default class FindPayslipByPeriod {
  payslipRepository: IPayslipRepository;
  constructor(payslipRepository: IPayslipRepository) {
    this.payslipRepository = payslipRepository;
  }

  execute(year: number, month: number) {
    return this.payslipRepository.findByPeriod(year, month);
  }
}
