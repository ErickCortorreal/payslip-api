import Payslip from "../../domain/entities/Payslip";

export default class CalculateTaxAmount {
  payslips: Payslip[];
  constructor(payslips: Payslip[]) {
    this.payslips = payslips;
  }

  execute(newTaxRate: number) {
    return this.payslips.map(p => p.calculateTaxAmount(newTaxRate));
  }
}
